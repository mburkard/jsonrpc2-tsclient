// deno-lint-ignore-file no-explicit-any
export class RequestObject {
    id: number | string;
    method: string;
    params?: object | any[];
    jsonrpc: string;

    constructor(
        id: number | string,
        method: string,
        params?: object | any[],
    ) {
        this.id = id;
        this.method = method;
        this.params = params;
        this.jsonrpc = "2.0";
    }
}

export class NotificationObject {
    method: string;
    params?: object | Array<any>;
    jsonrpc: string;

    constructor(method: string, params?: object | Array<any>) {
        this.method = method;
        this.params = params;
        this.jsonrpc = "2.0";
    }
}

export class ErrorObject {
    code: number;
    message: string;
    data?: any;

    constructor(code: number, message: string, data?: any) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}

export class ErrorResponseObject {
    id: number | undefined;
    error: ErrorObject;
    jsonrpc: string;

    constructor(error: ErrorObject, id?: number) {
        this.id = id;
        this.error = error;
        this.jsonrpc = "2.0";
    }
}

export class ResultResponseObject {
    id: number;
    result: any;
    jsonrpc: string;

    constructor(id: number, result: any) {
        this.id = id;
        this.result = result;
        this.jsonrpc = "2.0";
    }
}


export function isResultResponse(
    response: ResultResponseObject | ErrorResponseObject,
): response is ResultResponseObject {
    return "result"! in response;
}

export function isErrorResponse(
    response: ResultResponseObject | ErrorResponseObject,
): response is ResultResponseObject {
    return "error"! in response;
}
