// deno-lint-ignore-file no-explicit-any
import { AxiosRequestHeaders } from "axios";
import {
    ErrorResponseObject,
    RequestObject,
    ResultResponseObject,
} from "./objects.ts";
import { JSONRPCClient } from "./client.ts";
import { WebSocket } from "ws";

export class RPCWebSocketClient extends JSONRPCClient {
    url: string;
    headers: AxiosRequestHeaders;
    private ws: WebSocket;
    private readonly messageResolvers: Map<string | number, any>;

    constructor(url: string, headers: AxiosRequestHeaders | null = null) {
        super();
        this.url = url;
        this.headers = headers || {} as AxiosRequestHeaders;
        this.headers["Content-Type"] = "application/json";
        this.messageResolvers = new Map();
        this.ws = new WebSocket(this.url);
    }

    /**
     * Connect to WebSocket server.
     */
    connect() {
        if (this.ws) {
            this.ws.close();
        }
        this.ws.on("message", (data) => {
            const response = JSON.parse(data.toString());
            const requestId = response.id;
            const resolver = this.messageResolvers.get(requestId);
            if (resolver) {
                resolver(response);
                this.messageResolvers.delete(requestId);
            }
        });
    }

    /**
     * Close connection to WebSocket server.
     */
    close() {
        this.ws.close();
    }

    protected async sendAndGetJSON(
        request: RequestObject,
    ): Promise<ResultResponseObject | ErrorResponseObject> {
        const ws = this.ws;

        // Create a Promise to return the WebSocket message.
        const promise = new Promise<ResultResponseObject | ErrorResponseObject>((resolve, _reject) => {
            const onOpen = () => {
                this.messageResolvers.set(request.id, resolve);
                ws.send(JSON.stringify(request));
            };

            if (ws.readyState === WebSocket.OPEN) {
                // If the WebSocket is already open, send the request immediately.
                onOpen();
            } else {
                // If the WebSocket is still connecting, wait for it to open.
                ws.addEventListener("open", onOpen);
            }
        });
        return await promise;
    }
}
