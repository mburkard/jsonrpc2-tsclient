// deno-lint-ignore-file no-explicit-any
import {
    ErrorResponseObject,
    isResultResponse,
    RequestObject,
    ResultResponseObject,
} from "./objects.ts";
import { getErrorByCode, ServerError } from "./errors.ts";

export abstract class JSONRPCClient {
    public preCallHooks: CallableFunction[] = [];
    public preCallAsyncHooks: CallableFunction[] = [];
    private ids: Map<number, number> = new Map();

    private getId(): number {
        let max = 0;
        for (const it of this.ids.values()) {
            if (it > max) {
                max = it;
            }
        }
        const newId = max + 1;
        this.ids.set(newId, newId);
        return newId;
    }

    protected abstract sendAndGetJSON(
        request: RequestObject,
    ): Promise<ResultResponseObject | ErrorResponseObject>;

    async call(method: string, params?: any[] | object): Promise<any> {
        this.preCallHooks.map((it) => it());
        await Promise.all(this.preCallAsyncHooks.map((it) => it()));

        const request = new RequestObject(this.getId(), method, params);
        const data = await this.sendAndGetJSON(request);
        if (data.id !== undefined) {
            this.ids.delete(data.id);
        }
        if (isResultResponse(data)) {
            return data.result;
        } else if (data.error !== undefined) {
            throw new (getErrorByCode(data.error.code) || ServerError)(
                data.error,
            );
        } else {
            throw Error(`Invalid response: ${JSON.stringify(data)}`);
        }
    }
}
