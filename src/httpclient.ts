import {
    ErrorResponseObject,
    RequestObject,
    ResultResponseObject,
} from "./objects.ts";
import { JSONRPCClient } from "./client.ts";
import axios, { AxiosRequestHeaders } from "axios";

export class RPCHTTPClient extends JSONRPCClient {
    url: string;
    headers: AxiosRequestHeaders;

    constructor(url: string, headers: AxiosRequestHeaders | null = null) {
        super();
        this.url = url;
        this.headers = headers || {} as AxiosRequestHeaders;
        this.headers["Content-Type"] = "application/json";
    }

    protected async sendAndGetJSON(
        request: RequestObject,
    ): Promise<ResultResponseObject | ErrorResponseObject> {
        return (await axios.post(this.url, request, { headers: this.headers }))
            .data;
    }
}
