import { rpcClient, RPCHTTPClient } from "../src/index.ts";

const transport = new RPCHTTPClient("http://localhost:8000/api");

@rpcClient(transport, "math.", ["connect", "close"])
class MathClient {
    // @ts-ignore Implemented by decorator.
    async add(a: number, b: number): Promise<number> {}

    // @ts-ignore Implemented by decorator.
    async divide(a: number, b: number): Promise<number> {}
}

test("Result Integration test", async () => {
    const client = new MathClient();
    const sum = await client.add(2, 2);
    return expect(sum).toBe(4);
});

test("Error Integration test", async () => {
    const client = new MathClient();
    try {
        return await client.divide(0, 0);
    } catch (err) {
        return expect(err.message.slice(0, 6)).toBe("-32000");
    }
});
